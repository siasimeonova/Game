<?php declare(strict_types=1);

//namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Game;
use App\Configuration\Configurator;
use App\Tiles\Tile;

/**
 * Test for Game class.
 *
 * @author Sia Simeonova
 */
class GameTest extends TestCase{
    
    /**
     * @param array $boardCreated myltidimentional array that represents game board with three reel partitions.
     * @param int $expectedResult The value of the expected calculated pays.
     * @throws \ReflectionException
     * @dataProvider providerForTestGetPayForTile
     */

    public function testGetPayForTile(array $boardCreated, int $expectedResult){
        
        // Create test prerequisites
        $game = new Game();
        Configurator::configurateGame();
        
        $tile = Tile::createTile(1, "normal");

        $refGame   = new ReflectionObject($game);
        $board = $refGame->getProperty("board");
        $board->setAccessible(true);
        $board->setValue($game, $boardCreated);

        $lines = [
            [1,1,1,1,1],
            [0,0,0,0,0],
            [2,2,2,2,2]
            ];
        $game->setLines($lines);
        
        // Invoke the tested method
        $refMethos = $refGame->getMethod("getPayForTile");
        $refMethos->setAccessible(true);
        $refMethos->invoke($game, $tile);
        
        // Validate expected result
        $payProperty = $refGame->getProperty("pays");
        $payProperty->setAccessible(true);
        
        $this->assertEquals($expectedResult, $payProperty->getValue($game));
    }
    
    /**
     * Creates prerequisits for testGetPayForTile.
     * 
     * @return array created game board collection snd the expected calculated pays.
     */
    public function providerForTestGetPayForTile(){
        $tile = Tile::createTile(1, "normal");
        $secondTile = Tile::createTile(2, "normal");
        $thirdTile = Tile::createTile(3, "normal");
        
        $fiveOccurrences =  [
                    [$tile,$tile,$tile],
                    [$tile,$tile,$tile],
                    [$tile,$tile,$tile],
                    [$tile,$tile,$tile],
                    [$tile,$tile,$tile]
                ];
        $fourOccurrences = [
                    [$thirdTile,$secondTile,$tile],
                    [$tile,     $tile,      $tile],
                    [$tile,     $tile,      $tile],
                    [$tile,     $tile,      $tile],
                    [$tile,     $tile,      $thirdTile]
                ];
        
          $threeOccurrences = [
                    [$thirdTile, $tile,      $secondTile],
                    [$tile,      $tile,      $tile],
                    [$tile,      $tile,      $tile],
                    [$tile,      $secondTile,$tile],
                    [$secondTile,$secondTile,$thirdTile]
                ];
           $lessThenThree = [
                    [$tile, $secondTile,      $secondTile],
                    [$tile,      $tile,      $tile],
                    [$secondTile,$tile,      $secondTile],
                    [$tile,      $secondTile,$tile],
                    [$secondTile,$tile      ,$tile]
                ];
           $twoTilesPays = [
                    [$secondTile, $tile,      $secondTile],
                    [$secondTile,      $tile,      $tile],
                    [$secondTile,      $tile,      $tile],
                    [$secondTile,      $tile,$tile],
                    [$secondTile,$tile,$thirdTile]
                ];
        
        return [
            'test five' => [$fiveOccurrences, 1500],
            'test four' => [$fourOccurrences, 900],
            'test three' => [$threeOccurrences, 300],
            'less than three' => [$lessThenThree, 0],
            'pays for two tiles' => [$twoTilesPays, 600]
        ];
    }
    
}
