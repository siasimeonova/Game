<?php declare(strict_types=1);

namespace App;

use App\Configuration\Configurator;
use App\Service\PaysService;
use App\Reel\ReelSelector;
use App\Tiles\NormalTile;
use App\Tiles\Tile;

/**
 * Simple slot game main class
 *
 * @author Sia Simeonova
 */
class Game {
   
    public const NUMBER_REEL_IN_GAME = 5;

    /**
     * @var array The board of the game 
     */
    private $board = array();

    /**
     * @var int The sum of the pays for the game 
     */
    private $pays;
    
    private $stake = 1;
    
    private static $lines;

    public static function setLines($lines){
        self::$lines = $lines;
    }
    
    /**
     * The main point of the project. 
     * Here is configure the game and all other game activities are done.
     */
    public function spin(){

        // Set game configuration
        Configurator::configurateGame();
        $this->pays = 0;
        
        // Create game board
        echo 'SELECTED TILES:'. PHP_EOL;
        $this->createBoard();
        $this->printBoard();
       
        // Replace mystery tile with random normal tile
        if($this->replaceMystery()){
            echo 'REVEALED MYSTERY SYMBOLS:'. PHP_EOL;
            $this->printBoard();
        }
        
        // Calculate pays for the current game run
        $this->getPays();
        if(isset($this->pays) && $this->pays > 0){
            echo 'YOU GET : ' . $this->pays * $this->stake . "!" . PHP_EOL;
        }
    }
    
    /**
     * Add selected reel partitions for one game run.
     */
    protected function createBoard(){
        for($number = 0; $number < self::NUMBER_REEL_IN_GAME; $number++){
            $this->board[$number] = ReelSelector::getReelSelectedTiles($number);
        }
    }
    
    /**
     * Check the game board for mystery tiles and replace them 
     * with one ramdomly selected normal tile.
     * 
     * @return bool True if mistery tile has been found and replaced, false otherwise.
     */
    private function replaceMystery() : bool{
        $mysteryTile = Tile::getMysteryTile();

        $hasMistery = false;
        $randomTile = Tile::getRandomNormalTile();

        foreach ($this->board as &$reel){
            for($index = 0; $index < ReelSelector::COUNT_OF_TILES_IN_A_REEL; $index++){
                if($reel[$index] == $mysteryTile){
                    $reel[$index] = $randomTile;
                    $hasMistery = true;
                }
            }
        }

        return $hasMistery;
    }
    
    /**
     * Print the game board  to the terminal
     */
    private function printBoard(){
         for($tile = 0; $tile < ReelSelector::COUNT_OF_TILES_IN_A_REEL; $tile++){
            for($reel = 0; $reel < self::NUMBER_REEL_IN_GAME; $reel++){
                echo "|". $this->board[$reel][$tile]->getId();
            }
            echo "|" . PHP_EOL;
        }
    }
    
    /**
     * Calculate all pays for the game
     */
    private function getPays(){
        foreach (Tile::getNormalTiles() as $tile){
            $this->getPayForTile($tile);
        }  
    }
    
    /**
     * Check if they are lines for the given line and 
     * add to pays property each pay that has been found.
     * 
     * @param NormalTile $tile
     */
    private function getPayForTile(NormalTile $tile){
        foreach (self::$lines as $line){
            // will collect line occurrences in the line
            $foundedTile = 0;
            for($index = 0; $index < self::NUMBER_REEL_IN_GAME; $index++){
                if($this->board[$index][$line[$index]] == $tile){
                    
                    if($index > 0 && $this->board[$index-1][$line[$index-1]] == $tile ){
                                    $foundedTile ++;
                    } else if($index < self::NUMBER_REEL_IN_GAME - 2 && $this->board[$index+1][$line[$index+1]] == $tile){
                                     $foundedTile ++;
                    }
                }
            }
            
            if($foundedTile >= PaysService::MIN_SYMBOLS_IN_LINE_FOR_PAY){
                $this->pays += PaysService::getPay($tile->getId(), $foundedTile);
            }
        }
    }
               
}
