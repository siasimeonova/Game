<?php declare(strict_types=1);

namespace App\Reel;

use App\Tiles\Tile;

/**
 * Creates reels slices that starts from random index of the reel and include three tiles.
 *
 * @author Sia Simeonova
 */
class ReelSelector {
    
    public const COUNT_OF_TILES_IN_A_REEL = 3;

    private static $reels;
    
    /**
     * Creates random partition of the real on the given index
     * 
     * @return array The reel partition (three tiles) with random start
     */
    public static function getReelSelectedTiles($index): array {
        $reel = self::$reels[$index];
        
        $selected = array_slice($reel, array_rand($reel), self::COUNT_OF_TILES_IN_A_REEL);
        
        // add first two tiles if the random selected tile was the last one
        if(sizeof($selected) == 1){
            array_push($selected, $reel[0], $reel[1]);
        }
        
        // add first tiles if the random selected tile was the one before the last one
        elseif (sizeof($selected) == 2) {
            array_push($selected, $reel[0]);
        }
        
        return $selected;
    }

    /**
     * Setter for the reel class property. Use array with tile IDs and 
     * replace each id with Tile object.
     *  
     * @param array $reels Multidimentional array with infromation for tiles in reels.
     */
    public static function setReels(array $reels){
        foreach ($reels as &$reel)
        {
           for($index = 0; $index < sizeof($reel); $index++){
               $reel[$index] = Tile::getTileById($reel[$index]);
           }
        }

        self::$reels = $reels;
    }
    
}
