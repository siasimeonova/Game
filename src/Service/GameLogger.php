<?php declare(strict_types=1);

namespace App\Service;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * Logger for the game project
 *
 * @author Sia Simeonova
 */
class GameLogger extends Logger{
    
    private const LOG_FOLDER = "logs";
    private const LOG_FILE_NAME = "errors.log";
    private const LOG_FILE_PATH_AND_NAME = DIRECTORY_SEPARATOR . self::LOG_FOLDER . DIRECTORY_SEPARATOR . self::LOG_FILE_NAME;
    
    public function __construct($name){
    parent::__construct($name);
    
    // set default handler
    array_unshift($this->handlers, new StreamHandler(realpath("") . self::LOG_FILE_PATH_AND_NAME)); 
    }
}
