<?php declare(strict_types=1);

namespace App\Tiles;

/**
 * Represent mystery type tile
 *
 * @author Sia Simeonova
 */
class MysteryTile extends Tile{

}
