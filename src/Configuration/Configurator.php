<?php declare(strict_types=1);

namespace App\Configuration;

use App\Game;
use App\Service\PaysService;
use App\Reel\ReelSelector;
use App\Service\GameLogger;
use App\Tiles\Tile;
use App\Tiles\Exceptions\WrongTileTypeException;

/**
 * Encode configuration file and configure static properties
 *
 * @author Sia Simeonova
 */
abstract class Configurator{
    
    private const CONFIGS_FILE_NAME = "config.json";
    private const TILES_KEY = "tiles";
    private const REELS_KEY = "reels";
    private const LINES_KEY = "lines";
    private const PAYS_KEY = "pays";
    
    /**
     * @var array encoded json config data for all game elements 
     */
    private static $data;
    
    /**
     * @var type Simple object for errors logging
     */
    private static $logger;

    /**
     * Set all configuration for the game
     */
    public static function configurateGame(){
        self::$logger = new GameLogger(self::class);
        self::$data = self::getEncoded();

        try{
            Tile::setTileCollections(self::$data[self::TILES_KEY]);
        } catch (WrongTileTypeException $ex) {
            self::$logger->error($ex->getMessage());
            echo "Game cannot be played at the moment!";
            exit();
        }
        
        ReelSelector::setReels(self::$data[self::REELS_KEY][0]);
        Game::setLines(self::$data[self::LINES_KEY]);
        PaysService::setPays(self::$data[self::PAYS_KEY]);
    }
   
    /**
     * Encode json config file to an array and return it
     * 
     * @return array the encoded configs as an array
     */
    private static function getEncoded() :array{
        $data = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . self::CONFIGS_FILE_NAME); 
        return json_decode($data, true);
    }
    
}
