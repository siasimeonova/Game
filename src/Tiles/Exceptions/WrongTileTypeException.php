<?php declare(strict_types=1);

namespace App\Tiles\Exceptions;

/**
 * Exception for Tile module
 *
 * @author Sia Simeonova
 */
class WrongTileTypeException extends \Exception{
    
}
