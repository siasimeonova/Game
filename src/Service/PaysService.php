<?php declare(strict_types=1);

namespace App\Service;

/**
 * Holds values for all available pays in the game.
 * 
 * @autor Sia Simeonova
 */
abstract class PaysService{
    
    public const MIN_SYMBOLS_IN_LINE_FOR_PAY = 3;
    
    public const TILE_INDEX = 0;
    public const OCCURRENCE_INDEX = 1;
    public const PAY_INDEX = 2;

    /**
     * @var array the available pays in the game 
     */
    private static $pays = array();

    public static function setPays(array $pays){
        self::$pays = $pays;
    }
    
    /**
     *  Calculate pay for the tile and line
     *
     * @param int $tile Id of the tile, for which there is a line.
     * @param int $count Count of tile id occurrences in the line.
     * @return float calculated pay for the given tile and occurrences in the line.
     */
    public static function getPay(int $tile, int $count): float{
        foreach (self::$pays as $pay){
            if($pay[self::TILE_INDEX] == $tile && $pay[self::OCCURRENCE_INDEX] == $count)
            {
                return $pay[self::PAY_INDEX];
            }
        }
    }
}



