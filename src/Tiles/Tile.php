<?php declare(strict_types=1);

namespace App\Tiles;

use App\Tiles\Exceptions\WrongTileTypeException;

/**
 * Representation of tile object in the game.
 *
 * @author Sia Simeonova
 */
abstract class Tile {
    
    public const TILE_TYPE_NORMAL = "normal";
    public const TILE_TYPE_MYSTERY = "mystery";
    
    private const ID_INDEX = "id";
    private const TYPE_INDEX = "type";
    
    
    private const CONFIGURATION_ERROR_MSG = "Wrong tile type [ %s ] in the configuration!";

    /**
     * @var int ID of the symbol.
     */
    private $id;
    
    /**
     * @var array Collection of all tiles in the game.
     */
    private static $tilesCollection = array();
    
    /**
     * @var array Collection of all normal tiles in the game.
     */
    private static $normalTilesCollection = array();
    
    /**
     * @var MysteryTile unique mistery tile element in the game.
     */
    private static $mysteryTile;

    public function __construct($id){
        $this->id = $id;
    }
    
     public function getId() : int {
        return $this->id;         
    }

    /**
     * Sets tile collections.
     * 
     * @param array $tileElements array with data for all tiles in the game.
     */
    public static function setTileCollections(array $tileElements) {
        foreach ($tileElements as $tileData){
             $tile = Tile::createTile($tileData[self::ID_INDEX], $tileData[self::TYPE_INDEX]);
             self::$tilesCollection[$tile->id] = $tile;
             if($tile instanceof NormalTile){
                self::$normalTilesCollection[$tile->id] = $tile;
             }
        }
    }
    
    /**
     * @return \App\Tiles\Tile tile object from the tiles collection with the given id.
     */
    public static function getTileById($tileId) : Tile {
        return self::$tilesCollection[$tileId];
    }

    /**
     * @return \App\Tiles\Tile random normal tile object.
     */
    public static function getRandomNormalTile() : Tile {
        return self::$normalTilesCollection[array_rand(self::$normalTilesCollection)];
    }
    
    /**
     * @return array \App\Tiles\Tile all normal tiles in the game.
     */
    public static function getNormalTiles() : array{
        return self::$normalTilesCollection;
    }
    
    /**
     * @return type \App\Tiles\MisteryTile the mistery tile object in the game.
     */
    public static function getMysteryTile(){
        return self::$mysteryTile;
    }
    
    /**
     * Factory method for tile objects creation. 
     * 
     * Create and return tiles, set $mysteryTile,
     * in case tile with the given ID has been created return it 
     * instead of creation of new one.
     * 
     * @param int $id the ID of the tile.
     * @param string $type type of the tile.
     * @return \App\Tiles\Tile the created Tile object
     * @throws WrongTileTypeException if a wrong tile type is provided.
     */
    public static function createTile($id, $type) : Tile{
        if(isset(self::$tilesCollection[$id])){
            return self::$tilesCollection[$id];
        }
        
        if($type == self::TILE_TYPE_NORMAL){
            return new NormalTile($id);
        }elseif($type == self::TILE_TYPE_MYSTERY){
            self::$mysteryTile = new MysteryTile($id);
            return self::$mysteryTile;
        }else{
            throw new WrongTileTypeException(sprintf(self::CONFIGURATION_ERROR_MSG, $type));
        }
    }

}