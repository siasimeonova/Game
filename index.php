<?php

// Autoload files using the Composer autoloader.
require_once __DIR__ . '/vendor/autoload.php';


(new \App\Game())->spin();

