# Simple implementation of slot game.

This project implements simple slot game.

## Installation

Run composer install

## Start the game 

Run index.php

## Run unit tests

./vendor/bin/phpunit tests

## Configuring the game

config.json is in src\Configuration folder. 

Should be replaced with new configurations. 
Configuration is performed in the play method of the game.
