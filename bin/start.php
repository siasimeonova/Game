#!/usr/bin/env php
<?php

use App\Game;

require '../vendor/autoload.php';

(new Game())->spin();